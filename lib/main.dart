import 'package:flutter/material.dart';
import 'package:mama_recipe/application/app/my_app.dart';

void main() {
  runApp(const MyApp());
}