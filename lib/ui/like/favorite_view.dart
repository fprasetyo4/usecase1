import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mama_recipe/ui/like/favorite_view_model.dart';
import 'package:stacked/stacked.dart';

class FavoriteView extends ViewModelBuilderWidget<FavoriteViewModel> {
  @override
  FavoriteViewModel viewModelBuilder(BuildContext context) =>
      FavoriteViewModel();

  @override
  Widget builder(
      BuildContext context, FavoriteViewModel viewModel, Widget? child) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: const Text('Favorites'),
      ),
    );
  }
}
