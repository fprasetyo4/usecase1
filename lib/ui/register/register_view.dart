import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mama_recipe/ui/register/register_view_model.dart';
import 'package:stacked/stacked.dart';

class RegisterView extends ViewModelBuilderWidget<RegisterVIewModel> {
  @override
  RegisterVIewModel viewModelBuilder(BuildContext context) =>
      RegisterVIewModel();

  @override
  Widget builder(
      BuildContext context, RegisterVIewModel viewModel, Widget? child) {
    return Scaffold(
        // appBar: AppBar(
        //   backgroundColor: Colors.white,
        // ),
        body: Container(
      color: Colors.yellow,
      child: Form(
          child: Padding(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            const SizedBox(
              height: 100,
            ),
            const Text(
              'Let\'s Get Started!',
              textAlign: TextAlign.center,
              style: const TextStyle(
                  fontSize: 40,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            const Text(
              'create new account to access all feautures',
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 30,
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintText: 'Name',
                  prefixIcon: Icon(Icons.account_box_outlined),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white))),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintText: 'E-Mail',
                  prefixIcon: Icon(Icons.email),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white))),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintText: 'Phone Number',
                  prefixIcon: Icon(Icons.phone),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white))),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              decoration: const InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock_outline),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white))),
            ),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                  minimumSize: const Size(0, 50),
                  primary: Colors.white,
                  padding: const EdgeInsets.all(10),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
              child: const Text(
                'Create',
                style: TextStyle(color: Colors.yellow),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text('Already Have Account?'),
                GestureDetector(
                  onTap: viewModel.navigateToLoginView,
                  child: const Text(
                    'Log in here',
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            )
          ],
        ),
      )),
    ));
  }
}
