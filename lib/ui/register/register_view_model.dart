import 'package:mama_recipe/application/app/app.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class RegisterVIewModel extends BaseViewModel {
  final _navigationService = NavigationService();

  void navigateToLoginView() {
    _navigationService.navigateTo(Routes.loginView);
  }
}