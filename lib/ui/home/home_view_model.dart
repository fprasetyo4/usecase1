import 'package:mama_recipe/application/app/app.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HomeViewModel extends BaseViewModel{
  final _navigationSercvice = NavigationService();
   List<String> imageRecipe = [
    'https://res.cloudinary.com/dbpfwb5ok/image/upload/v1658960972/recipe/images/f0nr12ylu85dd0fjsgww.jpg',
    'https://res.cloudinary.com/dbpfwb5ok/image/upload/v1659255128/recipe/images/lqruzi0zb9cfmlu9vs2k.png',
    'https://res.cloudinary.com/dbpfwb5ok/image/upload/v1660197546/recipe/images/cpudpor1jbezdfu8mofx.png'

  ];

  void navigateToDetaiilView(){
    _navigationSercvice.navigateTo(Routes.detailView);
  }

  void navigateToFavoriteView(){
    _navigationSercvice.navigateTo(Routes.favoriteView);
  }
}