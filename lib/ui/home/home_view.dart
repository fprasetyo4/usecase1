import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mama_recipe/ui/home/home_view_model.dart';
import 'package:stacked/stacked.dart';

class HomeView extends ViewModelBuilderWidget<HomeViewModel> {
  @override
  HomeViewModel viewModelBuilder(BuildContext context) => HomeViewModel();

  @override
  Widget builder(BuildContext context, HomeViewModel viewModel, Widget? child) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            SizedBox(
              width: 70,
              child: TextField(
                cursorColor: Colors.yellow,
                decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide:
                            const BorderSide(color: Colors.transparent)),
                    filled: true,
                    hintText: 'Search Pasta, Bread, etc',
                    prefixIcon: IconButton(
                      icon: Icon(Icons.search),
                      color: Colors.black,
                      onPressed: () {},
                    ),
                    fillColor: Colors.yellow),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            GestureDetector(
              onTap: viewModel.navigateToFavoriteView,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  Padding(
                      padding: EdgeInsets.only(
                        left: 20,
                      ),
                      child: Icon(Icons.pin_invoke)),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 50),
                    child: Text(
                      'See detail your favorite recipe',
                      // textAlign: TextAlign.end,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              // padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              height: 150,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: viewModel.imageRecipe.length,
                itemBuilder: (contex, index) {
                  return GestureDetector(
                      onTap: () {},
                      child: Container(
                        margin: const EdgeInsets.only(left: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.network(viewModel.imageRecipe[index])),
                        // child: Text('cek 123'),
                      ));
                },
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text('Popular Recipes',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              height: 300,
              // color: Colors.yellow,
              child: ListView.builder(
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: GestureDetector(
                      onTap: viewModel.navigateToDetaiilView,
                      child: ListTile(
                        leading: Container(
                          height: 64,
                          width: 64,
                          decoration: BoxDecoration(
                              color: Colors.yellow,
                              borderRadius: BorderRadius.circular(10)),
                          child: Image.asset('assets/images/sendok.png'),
                        ),
                        title: Text('Makanan satu'),
                        subtitle: Row(
                          children: [
                            Image.asset('assets/images/star.png'),
                            const Padding(
                              padding: EdgeInsets.only(left: 5.0),
                              child: Text('4.6.'),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 5.0),
                              child: Text(
                                'Pasta',
                                style: TextStyle(color: Colors.grey),
                              ),
                            )
                          ],
                        ),
                        trailing: IconButton(
                            onPressed: () {}, icon: const Icon(Icons.star)),
                      ),
                    ),
                  );
                },
              ),
            ),
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                minimumSize: (const Size(30.0, 30.0)),
                elevation: 3,
                primary: Colors.yellow,
                padding: EdgeInsets.all(10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
              ),
              child: GestureDetector(
                onTap: () {},
                child: const Text(
                  'View More',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   items: const <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
      //     BottomNavigationBarItem(icon: Icon(Icons.add), label: 'Add'),
      //     // BottomNavigationBarItem(icon: Icon(Icons.chat), label: 'Chat'),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.account_circle), label: 'Profile'),
      //   ],
      //   currentIndex: 1,
      //   // onTap: (){},
      // ),
    );
  }
}
