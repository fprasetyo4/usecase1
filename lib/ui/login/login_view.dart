import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mama_recipe/ui/login/login_view_model.dart';
import 'package:stacked/stacked.dart';

class LoginView extends ViewModelBuilderWidget<LoginViewModel> {
  const LoginView({Key? key}) : super(key: key);

  @override
  LoginViewModel viewModelBuilder(BuildContext context) => LoginViewModel();

  @override
  Widget builder(
      BuildContext context, LoginViewModel viewModel, Widget? child) {
    return Scaffold(
      // appBar: AppBar(
      //   // title: const Text('Login'),
      //   centerTitle: true,
      //   backgroundColor: Colors.yellow[400],
      // ),
      body: Container(
        color: Colors.yellow[400],
        child: Form(
            child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView(
                  children: [
                    Image.asset(
                      'assets/images/logo.png',
                      height: 200,
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    const Text(
                      'Welcome!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      'Log in to your exiting account!',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20, color: Colors.black38),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    TextFormField(
                        decoration: const InputDecoration(
                            hintText: 'examplexxx@gmail.com',
                            prefixIcon: Icon(Icons.email_outlined),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)))),
                    const SizedBox(
                      height: 50,
                    ),
                    TextFormField(
                      obscureText: viewModel.passwordVisible,
                      decoration: InputDecoration(
                          hintText: 'Password',
                          prefixIcon: const Icon(Icons.lock_open_outlined),
                          enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          suffixIcon: IconButton(
                              onPressed: viewModel.onPressHintIcons,
                              icon: Icon(viewModel.passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off))),
                    ),
                    const SizedBox(
                      height: 100,
                    ),
                    ElevatedButton(
                        onPressed: viewModel.navigateToHome,
                        style: ElevatedButton.styleFrom(
                          minimumSize: const Size(0, 50),
                          primary: Colors.white,
                          padding: const EdgeInsets.all(10),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                        ),
                        child: const Text('Masuk',
                            style: TextStyle(
                              color: Colors.yellow,
                            ))),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        const Text('Don\'t have an account?'),
                        GestureDetector(
                          onTap: viewModel.navigateToRegisterView,
                          child: const Text(
                            'Sign up',
                            style: TextStyle(color: Colors.green),
                          ),
                        )
                      ],
                    )
                  ],
                ))),
      ),
    );
  }
}
