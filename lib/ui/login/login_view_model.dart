import 'package:mama_recipe/application/app/app.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class LoginViewModel extends BaseViewModel{
  final _navigationService = NavigationService();
  bool passwordVisible = false;

  void navigateToRegisterView(){
    _navigationService.navigateTo(Routes.registerView);
  }

  void navigateToHome(){
    _navigationService.navigateTo(Routes.homeView);
  }

  void onPressHintIcons() {
    passwordVisible = !passwordVisible;
    notifyListeners();
  }
}