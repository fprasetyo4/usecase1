import 'package:stacked/stacked.dart';

class DetailViewModel extends BaseViewModel {
  List<String> imageRecipe = [
    'https://res.cloudinary.com/dbpfwb5ok/image/upload/v1658960972/recipe/images/f0nr12ylu85dd0fjsgww.jpg',
    'https://res.cloudinary.com/dbpfwb5ok/image/upload/v1659255128/recipe/images/lqruzi0zb9cfmlu9vs2k.png',
    'https://res.cloudinary.com/dbpfwb5ok/image/upload/v1660197546/recipe/images/cpudpor1jbezdfu8mofx.png'
  ];

  List<String> bahan = ["bawang", "tomat", "timun"];

  String getNewLinseString() {
    StringBuffer sb =  StringBuffer();
    for(String line in bahan){
      sb.write('- $line, \n');
    }
    return sb.toString();
  }
}
