
import 'package:mama_recipe/ui/detail/detail_view.dart';
import 'package:mama_recipe/ui/home/home_view.dart';
import 'package:mama_recipe/ui/like/favorite_view.dart';
import 'package:mama_recipe/ui/login/login_view.dart';
import 'package:mama_recipe/ui/register/register_view.dart';
import 'package:stacked/stacked_annotations.dart';

@StackedApp(
  routes: [
    MaterialRoute(page: LoginView, initial: true),
    MaterialRoute(page: RegisterView),
    MaterialRoute(page: HomeView),
    MaterialRoute(page: DetailView),
    MaterialRoute(page: FavoriteView)
  ]
)

class AppSetup {
  
}